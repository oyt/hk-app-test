import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vant from 'vant';
import 'vant/lib/index.css';
import 'amfe-flexible/index.min.js'

import '@/assets/css/mian.css'
import VueAMap from 'vue-amap'


Vue.use(VueAMap)
VueAMap.initAMapApiLoader({
  key: '70bc2bb7dd52fcb4cd0f1474a563b778',
  plugin: ['AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PlaceSearch', 'AMap.Geolocation', 'AMap.Geocoder'],
  v: '1.4.4',
  uiVersion: '1.0'})
Vue.use(Vant);

import VCharts from 'v-charts'
Vue.use(VCharts)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
