import Vue from "vue";
import VueRouter from "vue-router";

//登陆
import Login from "../views/Login.vue";
//我的工作
import Work from "../views/Work.vue";
//我的提醒
import Remind from "../views/Remind.vue";
//事件详情
import RemindDetails from "../views/RemindDetails.vue";
//事件
import Event from "../views/Event.vue";

//我的档案
import File from "../views/File.vue";
//其它
import Other from "../views/Other.vue";

//上报
import Report from "../views/Report.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: Login
  },
  {
    path: "/work",
    name: "work",
    component: Work
  },
  {
    path: "/Remind",
    name: "remind",
    component: Remind,
  },
  {
    path: "/Event",
    component: Event,
  },
  {
    path: "/RemindDetails",
    component: RemindDetails,
  },
  {
    path: "/File",
    name: "file",
    component: File
  },
  {
    path: "/Other",
    name: "other",
    component: Other
  },
  {
    path: "/Report",
    name: "report",
    component: Report
  },
];

const router = new VueRouter({
  routes
});

export default router;
